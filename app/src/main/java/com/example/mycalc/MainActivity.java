package com.example.mycalc;

import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.TransitionSet;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

public class MainActivity extends AppCompatActivity {
    int i = 0;
    int max = 0;
    TextView tV0, tVtemp, operator;
    Button bt0,bt1,bt2,bt3,bt4,bt5,bt6,bt7,bt8,bt9;
    Button btSum,btSub,btMulti,btDiv,btPerc,btEqul,btBack,btOndown,btC,btPoint;
    //int oper = 10,operPressSum = 0,operPressSub = 0,operPressMulti = 0,operPressDiv = 0,operPressPoint = 0,operPressPerc = 0;
    double ans,lastnum;
    Boolean isPointer = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // view number
        tV0 = (TextView) findViewById(R.id.textView);
        tVtemp = (TextView)findViewById((R.id.textView2));
        tV0.setText("0");
        operator = (TextView)findViewById(R.id.textView3);
        // set object
        bt0 = (Button) findViewById(R.id.button17);
        bt1 = (Button) findViewById(R.id.button12);
        bt2 = (Button) findViewById(R.id.button13);
        bt3 = (Button) findViewById(R.id.button14);
        bt4 = (Button) findViewById(R.id.button8);
        bt5 = (Button) findViewById(R.id.button9);
        bt6 = (Button) findViewById(R.id.button10);
        bt7 = (Button) findViewById(R.id.button4);
        bt8 = (Button) findViewById(R.id.button5);
        bt9 = (Button) findViewById(R.id.button6);
        btSum    = (Button) findViewById(R.id.button15);//1
        btSub    = (Button) findViewById(R.id.button11);//2
        btMulti  = (Button) findViewById(R.id.button7);//3
        btDiv    = (Button) findViewById(R.id.button3);//4
        btPerc   = (Button) findViewById(R.id.button2);//5
        btEqul   = (Button) findViewById(R.id.button19);//6
        btBack   = (Button) findViewById(R.id.button16);//7
        btOndown = (Button) findViewById(R.id.button1);//8
        btC      = (Button) findViewById(R.id.button0);//9
        btPoint  = (Button) findViewById(R.id.button18);//0
        // set listener
        Button [] btns = {bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, btSum, btSub, btMulti, btDiv, btPerc, btEqul, btBack, btOndown, btC, btPoint};
        for(Button b : btns){
            b.setOnClickListener(myListerner);
        }



    }

    private Button.OnClickListener myListerner = new Button.OnClickListener(){
        @Override
        public void onClick(View v){
            Button btnTemp = (Button)findViewById(v.getId());
            String btnString = "";
            btnString = btnTemp.getText().toString();
            if(btnString.equals("+") || btnString.equals("-") || btnString.equals("*") || btnString.equals("/")){
                SetOperator(btnString);

            } else if(btnString.equals("C")){
                Clear();

            } else if(btnString.equals("=")){
                Calculate();

            } else {
                DisplayNum(btnString.toString());
            }

        }
    };
    private void SetOperator(String s){
        if(operator.getText().toString().equals("")){
            tVtemp.setText(tV0.getText().toString());
            tV0.setText("0");
            isPointer = false;
        }
        operator.setText(s);


    }
    private void Clear(){
        tV0.setText("0");
        tVtemp.setText("0");
        operator.setText("");
        isPointer = false;

    }

    private void DisplayNum(String s)
    {
        String str = "";

        str = tV0.getText().toString();
        BigDecimal bd = new BigDecimal(str);
        if(s.equals(".")) {
            if(isPointer)
                return;
            isPointer = true;
            tV0.setText(str + ".");
            return;
        }

        if(s.equals("+/-")) {

            bd = bd.negate();

        } else if(s.equals("Back")){
            if(str.length() > 0){
                str = str.substring(0,str.length()-1);
            }

            if(str.equals("") || str.equals("-")){
                str = "0";
            }
            if(str.indexOf('.')<0 || str.indexOf('.') == str.length()-1)
                isPointer = false;

            bd = new BigDecimal(str);

        } else if(s.equals("%")){
            try{
                bd = bd.divide(new BigDecimal(100));
            } catch (ArithmeticException e) {
                bd = bd.divide(new BigDecimal(100), 15, RoundingMode.HALF_UP);
            }

        } else{
            bd = new BigDecimal(str + s);
        }

        tV0.setText(bd.toString());
        CheckNumbers(bd);
    }

    private void Calculate(){

        BigDecimal num1 = new BigDecimal(tV0.getText().toString());
        BigDecimal num2 = new BigDecimal(tVtemp.getText().toString());
        String op = operator.getText().toString();

        if(op.equals("+")){
            try {
                num1 = num1.add(num2); //.setScale(15, RoundingMode.HALF_UP);
            } catch(ArithmeticException e){
                num1 = num1.add(num2).setScale(15,RoundingMode.HALF_UP);
            }

        } else if(op.equals("-")){
            try{
                num1 = num2.subtract(num1);
            } catch (ArithmeticException e){
            num1 = num2.subtract(num1).setScale(15,RoundingMode.HALF_UP);
            }

        } else if(op.equals("*")){
            try{
                num1 = num2.multiply(num1);
            } catch (ArithmeticException e) {
                num1 = num2.multiply(num1).setScale(15, RoundingMode.HALF_UP);
            }

        } else if(op.equals("/")){
            int comp = num1.compareTo(new BigDecimal("0"));
            if(comp==0){
                Clear();
                AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);
                alertDialog.setTitle("警告");
                alertDialog.setMessage("除以0");
                alertDialog.show();
                return;
            }

            //MathContext mc = new MathContext(5);
            try{
                num1 = num2.divide(num1);
            } catch (ArithmeticException e) {
                num1 = num2.divide(num1, 15, RoundingMode.HALF_UP);
            }

        }

        tV0.setText(num1.toString());
        tVtemp.setText("0");
        operator.setText("");
        isPointer = true;
        num1 = num1.stripTrailingZeros(); //.toPlainString();
        tV0.setText(num1.toPlainString());
        CheckNumbers(num1);

    }
    void CheckNumbers(BigDecimal bd){

        if(bd.scale()>15){
            bd = bd.setScale(15, BigDecimal.ROUND_DOWN);
            //tV0.setText(bd.toPlainString());
        }
        tV0.setText(bd.toPlainString());

        int comp0 = bd.compareTo(new BigDecimal("0"));
        int compUp = bd.compareTo(new BigDecimal("999999999999999"));

        if(compUp > 0){
            Clear();
            AlertDialog.Builder alertDialog= new AlertDialog.Builder(this);
            alertDialog.setTitle("警告");
            alertDialog.setMessage("超出可顯示最大位數");
            alertDialog.show();
        }

    }

}
